

var gulp = require('gulp');
var nunjucksRender = require('gulp-nunjucks-render');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var browserSync = require('browser-sync').create();
var gulpIgnore = require('gulp-ignore');
var $ = require('gulp-load-plugins')({rename: {'gulp-rev-delete-original':'revdel', 'gulp-if': 'if'}});
var condition = 'source/assets/views';

/* Tasks base */
gulp.task('copy', function() {
    return gulp.src(['source/assets/{img,font,player,video,docs}/**/*'], {base: 'source'})
        .pipe(gulp.dest('dist'));
});

gulp.task('clean', function() {
    return gulp.src('dist/', {read: false})
        .pipe($.clean());
});

// sass compile
gulp.task('styles', function() {
    gulp.src('source/assets/style/scss/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('source/assets/style/css/'))
        .pipe(browserSync.reload({
             stream: true
         }))
});

// browserSync
gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: 'source/'
        },
    })
});

/* Render do nunjunks */
gulp.task('render', function () {
    return gulp.src('source/assets/views/*.html')
    .pipe(nunjucksRender({
        path: ['source/assets/views/'] // String or Array
    }))
    .pipe(gulp.dest('source/'));
});


// verificação de alterações no arquivo
gulp.task('watch', ['browserSync', 'render', 'styles'], function (){
    gulp.watch('source/assets/style/scss/**/*.scss', ['styles']);
    gulp.watch('source/assets/views/**/*.html', ['render']);
    // Reloads the browser whenever HTML or JS files change
    gulp.watch('source/assets/**/*.html', browserSync.reload);
    gulp.watch('source/assets/script/**/*.js', browserSync.reload);
});




/* Minificação */
gulp.task('minify-js', function() {
  return gulp.src('source/*.js')
    .pipe($.uglify())
    .pipe(gulp.dest('dist/'))
});

gulp.task('minify-css', function() {
  return gulp.src('source/*.css')
    .pipe($.cssnano({safe: true}))
    .pipe(gulp.dest('dist/'))
});

gulp.task('minify-html', function() {
  return gulp.src('source/*.html')
    .pipe(gulpIgnore.exclude(condition))
    .pipe($.htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('dist/'))
});



/* Concatenação */
gulp.task('useref', function () {
    return gulp.src('source/*.html')
        .pipe(gulpIgnore.exclude(condition))
        .pipe($.useref())
        .pipe($.if('*.html', $.inlineSource()))
        .pipe($.if('*.html', $.htmlmin({collapseWhitespace: true})))
        .pipe($.if('*.js', $.uglify()))
        .pipe($.if('*.css', $.cssnano({safe: true})))
        .pipe(gulp.dest('dist'));
});




/* Imagens */
gulp.task('imagemin', function() {
    return gulp.src('source/assets/img/*')
        .pipe($.imagemin({
            progressive: true,
            svgoPlugins: [
                {removeViewBox: false},
                {cleanupIDs: false}
            ]
        }))
        .pipe(gulp.dest('dist/'));
});



/* Revisão de arquivos */
// gulp.task('rev', function(){
//   return gulp.src(['dist/**/*.{css,js,jpg,jpeg,png,svg}'])
//     .pipe($.rev())
//     .pipe($.revdel())
//     .pipe(gulp.dest('dist/'))
//     .pipe($.rev.manifest())
//     .pipe(gulp.dest('dist/'))
// })

// gulp.task('revreplace', ['rev'], function(){
//   return gulp.src(['dist/*.html', 'dist/**/*.css'])
//     .pipe($.revReplace({
//         manifest: gulp.src('dist/rev-manifest.json'),
//         replaceInExtensions: ['.html', '.js', '.css']
//     }))
//     .pipe(gulp.dest('dist/'));
// });



/* Alias */
gulp.task('minify', ['minify-js', 'minify-css', 'minify-html']);
gulp.task('build', $.sequence(['clean'], ['copy'], ['imagemin'], 'useref'));
gulp.task('default', $.sequence(['render', 'styles', 'browserSync', 'watch']));


