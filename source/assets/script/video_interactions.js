/**
 * Métodos para a interação do vídeo com os elementos da página
 *
 * @author	Fábio Hemkemaier (fabio@agileinterativa.com.br)
 * @version	1.0 (17/03/18)
 */

var hvVideoInteractions = function() {

    // Private methods

    /**
     * Initialize video player instance for interactions
     * @uses hvVideoInteractions.playerInstance
     * @return Void
     */
    var initPlayer = function() {
        // Set language
        mejs.i18n.language('pt');

        // Initialize Player
        $('#mainPlayer').mediaelementplayer({
            pluginPath: 'assets/player/',
            stretching: 'responsive',

            // Callback on success loading MediaElement
            success: function( mediaElement, originalNode, instance ) {
                // Set the player instance on global scope
                hvInteractions.playerInstance = mediaElement;

                // Listener to do actions during playing video
                mediaElement.addEventListener('timeupdate', function( e ) {
                    var currTime = mediaElement.getCurrentTime();

                    // Sets the chapter title
                    setChapterTitle( currTime );

                    // Sets the chapter subtitle
                    setChapterSubtitle( currTime );

                    // Sets complementar content
                    setComplementarContent( currTime );

                    // Sets locale on map
                    setLocaleOnMap( currTime );

                    // Highlight important dates
                    highlightImportantDates( currTime );
                }, false);

                // Listener to autoplay the video
                mediaElement.addEventListener('canplay', function( e ) {
                    // Player is ready
                    mediaElement.play();
                }, false);
            },

            // Callback on error loading MediaElement
            error: function( mediaElement, originalNode, instance ) {
                alert("Não foi possível carregar o player!\nAtualize a página para tentar novamente.");
                return false;
            }
        });
    };

    /**
     * Sets the chapter title from video current time
     * @param Float _currTime
     * @uses global.hvStoryBoard.chapterTitle
     * @return Void
     */
    var setChapterTitle = function( _currTime) {
        var sbElem = hvStoryBoard.chapterTitle; // Set the global storyboard element on a short variable
        var actualTitle = $('#hv-chapter-title').html();

        sbElem.forEach( function( _itemObj, _index) {
            if( _currTime >= _itemObj.time.start && _currTime <= _itemObj.time.end ) {
                // sets the chapter title and animate it
                if( actualTitle != _itemObj.value )
                    $('#hv-chapter-title').html( _itemObj.value );
            }
        });
    };

    /**
     * Sets the chapter subtitle from video current time
     * @param Float _currTime
     * @uses global.hvStoryBoard.chapterSubtitle
     * @return Void
     */
    var setChapterSubtitle = function( _currTime) {
        var sbElem = hvStoryBoard.chapterSubtitle; // Set the global storyboard element on a short variable
        var actualSubtitle = $('#hv-chapter-subtitle').html();

        sbElem.forEach( function( _itemObj, _index) {
            if( _currTime >= _itemObj.time.start && _currTime <= _itemObj.time.end ) {
                // sets the chapter subtitle and animate it
                if( actualSubtitle != _itemObj.value )
                    $('#hv-chapter-subtitle').html( _itemObj.value );
            }
        });
    };

    /**
     * Sets the complementar content from video current time
     * @param Float _currTime
     * @uses global.hvStoryBoard.complementarContent
     * @return Void
     */
    var setComplementarContent = function( _currTime) {
        var sbElem = hvStoryBoard.complementarContent; // Set the global storyboard element on a short variable
        var actualCompContentTitle = $('#hv-complementar-content-title').html();
        var actualCompContentSinopsis = $('#hv-complementar-content-sinopsis').html();
        var actualCompContentShowMoreBtn = false;

        sbElem.forEach( function( _itemObj, _index) {
            if( _currTime >= _itemObj.time.start && _currTime <= _itemObj.time.end ) {
                // sets the complementar content and animate it

                // Title
                if( actualCompContentTitle != _itemObj.title )
                    $('#hv-complementar-content-title').html( _itemObj.title );

                // Sinopsis
                if( actualCompContentSinopsis != _itemObj.sinopsis )
                    $('#hv-complementar-content-sinopsis').html( _itemObj.sinopsis );

                // Show more button
                if( actualCompContentShowMoreBtn != _itemObj.showMoreModalId ) {
                    if( _itemObj.showMoreModalId !== false )
                        $('#hv-complementar-content-modal-btn').attr('data-target', _itemObj.showMoreModalId ).show();
                    else
                        $('#hv-complementar-content-modal-btn').hide();
                }
                else
                    $('#hv-complementar-content-modal-btn').hide();
            }
        });
    };

    /**
     * Sets the marker on map
     * @param Float _currTime
     * @uses global.hvStoryBoard.locale, global.hvActualMarker
     * @return Void
     */
    var setLocaleOnMap = function( _currTime) {
        var sbElem = hvStoryBoard.locale; // Set the global storyboard element on a short variable

        sbElem.forEach( function( _itemObj, _index) {
            if( _currTime >= _itemObj.time.start && _currTime <= _itemObj.time.end ) {
                // Mark locale on map
                if( _currTime > 0 && hvActualMarker.name !== _itemObj.name ) {
                    hvMapMarkerPlot( _itemObj.lat, _itemObj.lng, _itemObj.name );

                    // Update actual locale on global variable
                    hvActualMarker = {
                        lat : _itemObj.lat,
                        lng : _itemObj.lng,
                        name : _itemObj.name
                    };
                }
            }
        });
    };

    /**
     * Highlight important dates blinking on timeline
     * @param Float _currTime
     * @uses global.hvStoryBoard.highlightDates
     * @return Void
     */
    var highlightImportantDates = function( _currTime) {
        var sbElem = hvStoryBoard.highlightDates; // Set the global storyboard element on a short variable

        // remove blink for all years
        $('.timeline-badge')
            .removeClass('animated')
            .removeClass('infinite')
            .removeClass('flash');

        sbElem.forEach( function( _itemObj, _index) {
            if( _currTime >= _itemObj.time.start && _currTime <= _itemObj.time.end ) {
                if( _itemObj.dates.length > 0 ) {
                    $.each( _itemObj.dates, function( _idxDate, _date ) {
                        var timelineElement = $('#hv-timeline-year-' + _date);

                        timelineElement
                            .addClass('animated')
                            .addClass('infinite')
                            .addClass('flash');
                    });
                }
            }
        });
    };

    // = - = - = - = - = - = - = - = - = - = - = - = - = - = - = \\

    // Public methods
    return {

        /**
         * Media Element Video player instance
         * @access public
         * @type Media Element Object
         */
        playerInstance : null

        /**
         * Initialize video player instance for interactions
         * @return MediaElement_Obj
         */
        , init : function() {
            initPlayer();

            // Trigger to pause video when modals are shown
            $('.hv-modals').on('show.bs.modal', function( e ) {
                hvInteractions.playerInstance.pause();
            });

            // Trigger to continue video when modals are hide
            $('.hv-modals').on('hidden.bs.modal', function( e ) {
                if( hvInteractions.playerInstance.getCurrentTime() > 0 )
                    hvInteractions.playerInstance.play();
            });
        }

    }; // /END public methods
};

var hvInteractions = new hvVideoInteractions();