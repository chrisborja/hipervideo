// ****** Scripts gerais*****//

//Inicia Tooltip fixa da timeline
$(function(){
  $('.timeline-badge').tooltip({ placement: 'bottom', trigger: 'manual' }).tooltip('show')

  if ($('.timeline-badge').attr('aria-describedby') === $('.tooltip').attr('id')){
    console.log(this);

  }else{
    console.log('nao deu certo')
  }

});

//muda classe da tooltip se estiver piscando
// let tooltipFlash = document.querySelectorAll('.animated');
// // let tooltipInner = document.querySelectorAll('.tooltip-inner');
// let tooltipDiv = document.querySelectorAll('.tooltip');
// console.log(tooltipDiv)


// // for (let i = 0; i < tooltipInner.length; i++) {
// //  console.log(tooltipInner[i])

// // }

// // console.log('tooltip trigger' + tooltipInner )
// // console.log('tooltip elemento' + tooltipDiv)



// **** GOOGLE MAPS CONFIGURATION **** \\

    // Global map object
    var hvMap = null;

    // Global map markers
    var hvMapMarkers = [];

    // Actual marker
    var hvActualMarker = {
        lat : null,
        lng : null,
        name : null,
    };

    /**
     * Sets the GoogleMaps div container sizes responsive
     * @return Void
     */
    var setsGMContainerResponsiveSize = function() {
        // Define GoogleMaps div container sizes
        var GM_ContainerWidth   = Math.round( $('.google-maps').outerWidth() ) - 1;
        var GM_ContainerHeight  = Math.round( $('.google-maps').outerHeight() ) - 1;

        if( GM_ContainerWidth <= 0 )
            GM_ContainerWidth = 289; // Fallback default width

        if( GM_ContainerHeight <= 0 )
            GM_ContainerHeight = 217; // Fallback default height

        // Set correct GoogleMaps div container sizes
        $('#hv-map').attr('style','width: ' + GM_ContainerWidth + 'px; height: ' + GM_ContainerHeight + 'px');
    };

    /**
     * Open in a new window a larger Google Maps Marker
     * @uses hvActualMarker
     * @return New Window Opens
     */
    var viewGMLargerMapPlace = function() {
        // Replaces white spaces with plus
        var sanitizedName = hvActualMarker.name.replace(/\s/g,'+');

        // Sets Larger Map URL
        var placeUrl = "https://www.google.com/maps/search/?api=1&query=" + hvActualMarker.lat + "," + hvActualMarker.lng;

        // Open larger map url in a new window
        window.open( placeUrl );

        // Pause video
        hvInteractions.playerInstance.pause();
    };

    /**
     * Initialize GoogleMap object
     * @uses hvMap
     * @return Void
     */
    var hvMapInit = function() {

        // Sets the GoogleMaps div container responsive sizes
        setsGMContainerResponsiveSize();

        // Definition and settings of Map Object
        var objHtmlMap = document.getElementById('hv-map'),
            mapOptions = {
    			mapTypeControl: false,
                mapTypeControlOptions: {
    				style: google.maps.MapTypeControlStyle.DEFAULT,
    				position: google.maps.ControlPosition.DEFAULT
    			},
    			zoom: 12,
    			zoomControl: false,
    			zoomControlOptions: {
    				style: google.maps.ZoomControlStyle.DEFAULT,
    				position: google.maps.ControlPosition.DEFAULT
    			},
    			panControl: false,
    			streetViewControl: false,
    			fullscreenControl: false,
    			scaleControl: false,
    			overviewMapControl: false,
    			scrollwheel: false,
    			center: new google.maps.LatLng(-13.7030384,-69.6833516),
    			styles: [
                  {
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#212121"
                      }
                    ]
                  },
                  {
                    "elementType": "labels.icon",
                    "stylers": [
                      {
                        "visibility": "off"
                      }
                    ]
                  },
                  {
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#757575"
                      }
                    ]
                  },
                  {
                    "elementType": "labels.text.stroke",
                    "stylers": [
                      {
                        "color": "#212121"
                      }
                    ]
                  },
                  {
                    "featureType": "administrative",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#757575"
                      }
                    ]
                  },
                  {
                    "featureType": "administrative.country",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#9e9e9e"
                      }
                    ]
                  },
                  {
                    "featureType": "administrative.land_parcel",
                    "stylers": [
                      {
                        "visibility": "off"
                      }
                    ]
                  },
                  {
                    "featureType": "administrative.locality",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#bdbdbd"
                      }
                    ]
                  },
                  {
                    "featureType": "poi",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#757575"
                      }
                    ]
                  },
                  {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#181818"
                      }
                    ]
                  },
                  {
                    "featureType": "poi.park",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#616161"
                      }
                    ]
                  },
                  {
                    "featureType": "poi.park",
                    "elementType": "labels.text.stroke",
                    "stylers": [
                      {
                        "color": "#1b1b1b"
                      }
                    ]
                  },
                  {
                    "featureType": "road",
                    "elementType": "geometry.fill",
                    "stylers": [
                      {
                        "color": "#2c2c2c"
                      }
                    ]
                  },
                  {
                    "featureType": "road",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#8a8a8a"
                      }
                    ]
                  },
                  {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#373737"
                      }
                    ]
                  },
                  {
                    "featureType": "road.highway",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#3c3c3c"
                      }
                    ]
                  },
                  {
                    "featureType": "road.highway.controlled_access",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#4e4e4e"
                      }
                    ]
                  },
                  {
                    "featureType": "road.local",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#616161"
                      }
                    ]
                  },
                  {
                    "featureType": "transit",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#757575"
                      }
                    ]
                  },
                  {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#000000"
                      }
                    ]
                  },
                  {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#3d3d3d"
                      }
                    ]
                  }
                ]
    		};

        // Global map object
        hvMap = new google.maps.Map( objHtmlMap, mapOptions );

        // Listener to resize window
        google.maps.event.addDomListener(window, "resize", function() {
            setsGMContainerResponsiveSize();

            var center = hvMap.getCenter();
            google.maps.event.trigger( hvMap, "resize" );
            hvMap.setCenter( center );
        });

        // View larger map button trigger
        $('#hv-larger-map').click( function( _ev ) {
            viewGMLargerMapPlace();
        });
    }

    /**
     * Remove all map markers
     * @uses hvMapMarkers
     * @return Void
     */
    var hvMapClearAllMarkers = function() {
        for( var i=0; i<hvMapMarkers.length; i++ ) {
            hvMapMarkers[i].setMap(null);
        }
    }

    /**
     * Plot a marker on Map
     * @param Float _lat
     * @param Float _lng
     * @param String _localeName
     * @uses hvMap, hvMapMarkers, hvActualMarker
     * @return Void
     */
    var hvMapMarkerPlot = function( _lat, _lng, _localeName ) {
        if( hvMap != null ) {
            // Remove all old markers
            hvMapClearAllMarkers();

            var coordinates = new google.maps.LatLng( _lat, _lng );

            // Set a marker Object
            var marker = new google.maps.Marker({
                map: hvMap, // Global Map Object
                position: coordinates,
                draggable: false,
            });

            // Set an infowindow Object
            var infowindow = new google.maps.InfoWindow({
                content: '<p style="color:black !important">' + _localeName + '</p>'
            });

            // Centralize map on coordinate
            hvMap.setCenter( coordinates );

			// Show infowindow
            infowindow.open( hvMap, marker );

            // Responsive infowindow on window resize
            google.maps.event.addDomListener(window, "resize", function() {
                infowindow.open( hvMap, marker );
            });

            // Add active marker on global marker array
            hvMapMarkers.push( marker );

            // Set actual marker for global purposes
            hvActualMarker = {
                lat : _lat,
                lng : _lng,
                name : _localeName
            };
        }
        else {
            // Fallback to force initialize map
            hvMapInit();
            hvMapMarkerPlot( _lat, _lng, _localeName );
        }
    };

// /END: GOOGLE MAPS \\

// Inicializa as interações do video
hvInteractions.init();