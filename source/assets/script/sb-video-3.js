/**
 * This is storyboard from video 3
 * @access protected
 * @type Object
 */
var hvStoryBoard = {
    // Titles
    chapterTitle : [
        {
            time : { start : 0, end : 187.561 },
            value : "Provocação"
        },
        {
            time : { start : 187.562, end : 275.3 },
            value : "Mudança do paradigma de formação do médico"
        },
        {
            time : { start : 275.4, end : 400.857 },
            value : "A formação do médico no PMM"
        },
        {
            time : { start : 400.858, end : 452.325 },
            value : "Abertura de novos cursos de medicina"
        },
        {
            time : { start : 452.326, end : 821.820 },
            value : "Novas diretrizes curriculares nacionais de medicina"
        },
        {
            time : { start : 821.821, end : 962.794 },
            value : "Rede Ensino-Serviço"
        },
        {
            time : { start : 962.795, end : 1011.426 },
            value : "Residência na lei do Mais Médicos"
        },
        {
            time : { start : 1011.427, end : 1300.256 },
            value : "Residência como fator de sustentabilidade da ABS"
        },
        {
            time : { start : 1300.257, end : 1638 },
            value : "Sustentabilidade do Programa Mais Médicos"
        },
    ],

    // Subtitles
    chapterSubtitle : [
        {
            time : { start : 0, end : 187.561 },
            value : "A cerimônia do  Lava-pés em Caruaru"
        },
        {
            time : { start : 187.562, end : 236.818 },
            value : "O paradigma tradicional na formação médica"
        },
        {
            time : { start : 236.819, end : 275.3 },
            value : "Histórico da formação médica"
        },
        {
            time : { start : 275.4, end : 400.857 },
            value : "O eixo de formação do PMM e sua importância"
        },
        {
            time : { start : 400.858, end : 423.296 },
            value : "O processo de abertura de novo cursos de Medicina "
        },
        {
            time : { start : 423.297, end : 452.325 },
            value : "Internato Rural do Curso de Medicina de Manaus"
        },
        {
            time : { start : 452.326, end : 510.967 },
            value : "Diretrizes Curriculares dos Cursos de Medicina"
        },
        {
            time : { start : 510.968, end : 610.317 },
            value : "Metodologias ativas de ensino e aprendizagem"
        },
        {
            time : { start : 610.318, end : 648.897 },
            value : "Autonomia do aluno no processo de aprendizagem"
        },
        {
            time : { start : 648.898, end : 672.212 },
            value : "Inserção no campo de prática desde o início do curso "
        },
        {
            time : { start : 672.213, end : 821.820 },
            value : ""
        },
        {
            time : { start : 821.821, end : 835.95 },
            value : "O que é rede de ensino-serviço"
        },
        {
            time : { start : 835.96, end : 845.134 },
            value : "Contratos Organizativos de Ação Pública Ensino-Saúde"
        },
        {
            time : { start : 845.135, end : 962.794 },
            value : ""
        },
        {
            time : { start : 962.795, end : 978.267 },
            value : "As mudanças na Residência Médica"
        },
        {
            time : { start : 978.268, end : 993.032 },
            value : "Vantagens oferecidas aos médicos residentes"
        },
        {
            time : { start : 993.033, end : 1011.426 },
            value : ""
        },
        {
            time : { start : 1011.427, end : 1112.860 },
            value : "Residência médica como fator de fixação do médico "
        },
        {
            time : { start : 1112.861, end : 1300.256 },
            value : "Integração do residente à rede ensino-serviço"
        },
        {
            time : { start : 1300.257, end : 1638 },
            value : "Interiorização da graduação e Residência"
        },
    ],

    // Complementar content
    complementarContent : [
        {
            time : { start : 0, end : 84.583 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 84.584, end : 111.193 },
            title : "Destaque",
            sinopsis : "Link para outros vídeos no Saiba mais.",
            showMoreModalId : "#modalComplementarContent_1",
        },
        {
            time : { start : 111.194, end : 187.561 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 187.562, end : 213.086 },
            title : "Paradigma",
            sinopsis : "O paradigma em sentido amplo é um conjunto de crenças, valores, técnicas etc. que influenciam o pensamento e a ação de uma comunidade, de um grupo, ou até mesmo, de um indivíduo.",
            showMoreModalId : "#modalComplementarContent_2",
        },
        {
            time : { start : 213.087, end : 236.818 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 236.819, end : 262.386 },
            title : "Destaque",
            sinopsis : "A proposta curricular desenvolvida nos Estados Unidos no início do século XX por Flexner foi adotada pelo Brasil.",
            showMoreModalId : "#modalComplementarContent_3",
        },
        {
            time : { start : 262.387, end : 275.39 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 275.4, end : 362.31 },
            title : "Eixo Formação",
            sinopsis : "O Eixo Formação possui um importante papel na sustentabilidade do Programa Mais Médicos e da Atenção Básica à Saúde. ",
            showMoreModalId : "#modalComplementarContent_4",
        },
        {
            time : { start : 362.32, end : 400.857 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 400.858, end : 423.296 },
            title : "Destaque",
            sinopsis : "O Programa Mais Médicos determinou a criação de novos cursos de medicina distantes dos grandes centros.",
            showMoreModalId : "#modalComplementarContent_5",
        },
        {
            time : { start : 423.297, end : 452.325 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 452.326, end : 474.973 },
            title : "Destaque",
            sinopsis : "As Diretrizes Curriculares Nacionais (DCNs) de um curso são normas obrigatórias que orientam o planejamento curricular das escolas e dos sistemas de ensino. Elas são discutidas, concebidas e fixadas pelo Conselho Nacional de Educação (CNE).",
            showMoreModalId : false,
        },
        {
            time : { start : 474.974, end : 510.967 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 510.968, end : 538.703 },
            title : "Destaque",
            sinopsis : "Metodologias ativas visam estimular o interesse do estudante de forma a aprofundar o estudo, a pesquisar, refletir e analisar alternativas para tomada de decisão, assumir a responsabilidade pela própria aprendizagem...",
            showMoreModalId : "#modalComplementarContent_6",
        },
        {
            time : { start : 538.704, end : 561.101 },
            title : "Destaque",
            sinopsis : "Laboratório Morfofuncional",
            showMoreModalId : "#modalComplementarContent_7",
        },
        {
            time : { start : 561.102, end : 580.245 },
            title : "Destaque",
            sinopsis : "Laboratório de sensibilidades, habilidades e expressão.",
            showMoreModalId : "#modalComplementarContent_8",
        },
        {
            time : { start : 580.246, end : 597.804 },
            title : "Destaque",
            sinopsis : "PBL - Aprendizado baseado em problemas",
            showMoreModalId : "#modalComplementarContent_9",
        },
        {
            time : { start : 597.805, end : 610.317 },
            title : "Destaque",
            sinopsis : "Inserção no campo de prática",
            showMoreModalId : "#modalComplementarContent_10",
        },
        {
            time : { start : 610.318, end : 648.897 },
            title : "Autonomia",
            sinopsis : "O estado de autonomia é aquele em que o indivíduo amplia a consciência de si na interação com o outro, posicionando-se como sujeito dos próprios atos.",
            showMoreModalId : "#modalComplementarContent_11",
        },
        {
            time : { start : 648.898, end : 672.212 },
            title : "Destaque",
            sinopsis : "A inserção no campo de prática desde o início do curso possibilita ao futuro médico vivenciar a amplitude da Atenção Básica. ",
            showMoreModalId : false,
        },
        {
            time : { start : 672.213, end : 696.820 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 696.821, end : 710.542 },
            title : "Destaque",
            sinopsis : "Organização dos Serviços de Saúde/ Níveis de Atenção em Saúde",
            showMoreModalId : "#modalComplementarContent_12",
        },
        {
            time : { start : 710.543, end : 740.071 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 740.072, end : 758.840 },
            title : "LOCUS",
            sinopsis : "Locus é uma palavra latina que significa lugar , posição, espaço ou local .",
            showMoreModalId : false,
        },
        {
            time : { start : 758.841, end : 835.95 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 835.96, end : 845.134 },
            title : "COAPES",
            sinopsis : "O Contrato Organizativo de Ação Pública Ensino-Saúde – COAPES é um Contrato firmado entre os gestores municipais e/ou estaduais do SUS com vistas à utilização da rede de Saúde como campo de prática…",
            showMoreModalId : "#modalComplementarContent_13",
        },
        {
            time : { start : 845.135, end : 962.794 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 962.795, end : 978.267 },
            title : "Residência Médica",
            sinopsis : "A residência médica corresponde a uma especialização lato sensu e se caracteriza por ser uma modalidade de treinamento em serviço em que ocorre: ",
            showMoreModalId : "#modalComplementarContent_14",
        },
        {
            time : { start : 978.268, end : 993.032 },
            title : "Destaque",
            sinopsis : "A Lei do Mais Médicos garante alguns benefícios aos participantes do Programa. Ela mantém as vantagens oferecidas pelo Programa de Valorização dos Profissionais da Atenção Básica (PROVAB), anterior ao PMM, visando atrair os médicos. ",
            showMoreModalId : "#modalComplementarContent_15",
        },
        {
            time : { start : 993.033, end : 1011.426 },
            title : "Formação em serviço",
            sinopsis : "Processo de apropriação de conhecimentos e desenvolvimento de competências necessários ao exercício profissional, que utiliza o ambiente profissional como espaço de reflexão e intervenção...",
            showMoreModalId : "#modalComplementarContent_16",
        },
        {
            time : { start : 1011.427, end : 1070.860 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 1070.861, end : 1112.860 },
            title : "Preceptoria",
            sinopsis : "Preceptoria é a ação de acompanhar e orientar na educação de algum conhecimento, seja ele das áreas de saúde, humanas, social ou qualquer outra...",
            showMoreModalId : "#modalComplementarContent_17",
        },
        {
            time : { start : 1112.861, end : 1194.735 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 1194.735, end : 1226.59 },
            title : "Saúde do território",
            sinopsis : "O território pode ser entendido como espaço delimitado geograficamente, por meio de critérios de geoprocessamento. Na saúde, no entanto, assume uma definição mais complexa, ou seja, como relações sociais projetadas em determinado espaço...",
            showMoreModalId : "#modalComplementarContent_18",
        },
        {
            time : { start : 1226.6, end : 1317.690 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 1317.691, end : 1334.582 },
            title : "",
            sinopsis : "O Programa Mais Médicos vem impulsionando a fixação dos residentes.",
            showMoreModalId : "#modalComplementarContent_19",
        },
        {
            time : { start : 1334.583, end : 1528.275 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 1528.276, end : 1595.426 },
            title : "Destaque",
            sinopsis : "O Programa Mais Médicos possui um importante componente estrutural que visa garantir o fortalecimento da saúde da comunidade. ",
            showMoreModalId : false,
        },
        {
            time : { start : 1595.427, end : 1638 },
            title : "",
            sinopsis : "• Acesse a Cartilha do Mais Médicos e tenha uma visão panorâmica do Programa no período de 2013 ao primeiro semestre de 2017. ",
            showMoreModalId : false,
        },
    ],

    // Locale
    locale : [
        {
            time : { start : 0, end : 111.193 },
            lat : -8.1875808,
            lng : -36.1562501,
            name : 'Caruaru - PE',
        },
        {
            time : { start : 111.194, end : 125.9 },
            lat : -8.0555435,
            lng : -34.8790755,
            name : 'Recife - PE',
        },
        {
            time : { start : 126, end : 187.561 },
            lat : -8.1875808,
            lng : -36.1562501,
            name : 'Caruaru - PE',
        },
        {
            time : { start : 187.562, end : 213.086 },
            lat : -15.721751,
            lng : -48.0073977,
            name : 'Brasília - DF',
        },
        {
            time : { start : 213.087, end : 236.818 },
            lat : -9.3727815,
            lng : -40.5275588,
            name : 'Petrolina - PE',
        },
        {
            time : { start : 236.819, end : 262.386 },
            lat : -15.721751,
            lng : -48.0073977,
            name : 'Brasília - DF',
        },
        {
            time : { start : 262.387, end : 275.39 },
            lat : -9.3727815,
            lng : -40.5275588,
            name : 'Petrolina - PE',
        },
        {
            time : { start : 275.4, end : 362.31 },
            lat : -15.721751,
            lng : -48.0073977,
            name : 'Brasília - DF',
        },
        {
            time : { start : 362.32, end : 387.094 },
            lat : -8.1875808,
            lng : -36.1562501,
            name : 'Caruaru - PE',
        },
        {
            time : { start : 387.095, end : 400.857 },
            lat : -15.721751,
            lng : -48.0073977,
            name : 'Brasília - DF',
        },
        {
            time : { start : 400.858, end : 423.296 },
            lat : -9.3727815,
            lng : -40.5275588,
            name : 'Petrolina - PE',
        },
        {
            time : { start : 423.297, end : 452.325 },
            lat : -3.0446598,
            lng : -60.0371438,
            name : 'Manaus - AM',
        },
        {
            time : { start : 452.326, end : 474.973 },
            lat : -9.3727815,
            lng : -40.5275588,
            name : 'Petrolina - PE',
        },
        {
            time : { start : 474.974, end : 510.967 },
            lat : -3.0446598,
            lng : -60.0371438,
            name : 'Manaus - AM',
        },
        {
            time : { start : 510.968, end : 538.703 },
            lat : -9.3727815,
            lng : -40.5275588,
            name : 'Petrolina - PE',
        },
        {
            time : { start : 538.704, end : 962.794 },
            lat : -8.1875808,
            lng : -36.1562501,
            name : 'Caruaru - PE',
        },
        {
            time : { start : 962.795, end : 1011.426 },
            lat : -9.3727815,
            lng : -40.5275588,
            name : 'Petrolina - PE',
        },
        {
            time : { start : 1011.427, end : 1024.272 },
            lat : -8.1875808,
            lng : -36.1562501,
            name : 'Caruaru - PE',
        },
        {
            time : { start : 1024.273, end : 1226.59 },
            lat : -9.3727815,
            lng : -40.5275588,
            name : 'Petrolina - PE',
        },
        {
            time : { start : 1226.6, end : 1248.496 },
            lat : -8.5081921,
            lng : -39.31902,
            name : 'Cabrobó - PE',
        },
        {
            time : { start : 1248.497, end : 1352.767 },
            lat : -9.3727815,
            lng : -40.5275588,
            name : 'Petrolina - PE ',
        },
        {
            time : { start : 1352.768, end : 1364.236 },
            lat : -22.8777203,
            lng : -43.2475741,
            name : 'Fiocruz - Rio de Janeiro - RJ',
        },
        {
            time : { start : 1364.237, end : 1388.260 },
            lat : -9.3727815,
            lng : -40.5275588,
            name : 'Petrolina - PE ',
        },
        {
            time : { start : 1388.261, end : 1401.107 },
            lat : -8.1875808,
            lng : -36.1562501,
            name : 'Caruaru - PE',
        },
        {
            time : { start : 1401.108, end : 1416.497 },
            lat : -9.3727815,
            lng : -40.5275588,
            name : 'Petrolina - PE ',
        },
        {
            time : { start : 1416.498, end : 1433.597 },
            lat : -23.8046745,
            lng : -46.6718337,
            name : 'São Bernardo do Campo - SP',
        },
        {
            time : { start : 1433.598, end : 1456.704 },
            lat : -9.3727815,
            lng : -40.5275588,
            name : 'Petrolina - PE ',
        },
        {
            time : { start : 1456.705, end : 1476.807 },
            lat : -22.8777203,
            lng : -43.2475741,
            name : 'Fiocruz - Rio de Janeiro - RJ',
        },
        {
            time : { start : 1476.808, end : 1498.454 },
            lat : -30.1087957,
            lng : -51.3172247,
            name : 'Porto Alegre - RS',
        },
        {
            time : { start : 1498.455, end : 1506.754 },
            lat : -9.3727815,
            lng : -40.5275588,
            name : 'Petrolina - PE ',
        },
        {
            time : { start : 1506.755, end : 1517.556 },
            lat : -15.721751,
            lng : -48.0073977,
            name : 'Brasília - DF',
        },
        {
            time : { start : 1517.557, end : 1595.426 },
            lat : -9.3727815,
            lng : -40.5275588,
            name : 'Petrolina - PE ',
        },
        {
            time : { start : 1595.427, end : 1613.445 },
            lat : -23.8046745,
            lng : -46.6718337,
            name : 'São Bernardo do Campo - SP',
        },
    ],

    // Highlight dates
    highlightDates : [
        {
            time : { start : 0, end: 236.818 },
            dates : []
        },
        {
            time : { start : 236.819, end: 262.386 },
            dates : [1906]
        },
        {
            time : { start : 262.387, end: 275.39 },
            dates : [1968, 1988]
        },
        {
            time : { start :275.4, end: 362.31 },
            dates : [1988, 2003]
        },
        {
            time : { start : 362.32, end: 452.325 },
            dates : []
        },
        {
            time : { start : 452.326, end: 474.973 },
            dates : [2014]
        },
        {
            time : { start : 474.974, end: 835.95 },
            dates : []
        },
        {
            time : { start : 835.96, end: 845.134 },
            dates : [2015]
        },
        {
            time : { start : 845.135, end: 962.794 },
            dates : []
        },
        {
            time : { start : 962.795, end: 978.267 },
            dates : [2013, 2016]
        },
        {
            time : { start : 978.268, end: 1334.582 },
            dates : []
        },
        {
            time : { start : 1334.583, end: 1364.236 },
            dates : [2013, 2016]
        },
        {
            time : { start : 1364.237, end: 1638 },
            dates : []
        },
    ],
};