/**
 * This is storyboard from video 2
 * @access protected
 * @type Object
 */
var hvStoryBoard = {
    // Titles
    chapterTitle : [
        {
            time : { start : 0, end: 27.234 },
            value : "Provimento Emergencial"
        },
        {
            time : { start : 27.235, end: 262.010 },
            value : "Chegada dos médicos do PMM"
        },
        {
            time : { start : 262.011, end: 292.24 },
            value : "Contratação dos médicos"
        },
        {
            time : { start : 292.25, end: 479.352 },
            value : "Importância da presença do médico para a ABS"
        },
        {
            time : { start : 479.353, end: 511.927 },
            value : "Candidatura e compromissos dos municípios para receber o PMM"
        },
        {
            time : { start : 511.928, end: 693.191 },
            value : "Impactos da presença do médico em locais de difícil acesso"
        },
        {
            time : { start : 693.192, end: 867.240 },
            value : "Aperfeiçoamento profissional no PMM"
        },
        {
            time : { start : 867.241, end: 1304.885 },
            value : "Os participantes do PMM no sistema Ensino Serviço"
        },
        {
            time : { start : 1304.886, end: 1470.050 },
            value : "Perfil dos médicos do PMM"
        },
        {
            time : { start : 1470.051, end: 1566 },
            value : "Impactos dos vínculos na Atenção Básica à Saúde"
        },
    ],

    // Subtitles
    chapterSubtitle : [
        {
            time : { start : 0, end: 109.358 },
            value : ""
        },
        {
            time : { start : 109.359, end: 135.300 },
            value : "Acolhimento do Ministério da Saúde"
        },
        {
            time : { start : 135.301, end: 151.984 },
            value : "Recepção dos médicos nos municípios"
        },
        {
            time : { start : 151.985, end: 179.678 },
            value : "Integração à Comunidade"
        },
        {
            time : { start : 193.359, end: 215.923 },
            value : "Impactos sobre o atendimento médico"
        },
        {
            time : { start : 215.924, end: 262.010 },
            value : "Atendimento domiciliar"
        },
        {
            time : { start : 262.011, end: 292.24 },
            value : "Critérios de contratação"
        },
        {
            time : { start : 292.25, end: 349.931 },
            value : "Fixação do profissional"
        },
        {
            time : { start : 349.932, end: 381.880 },
            value : ""
        },
        {
            time : { start : 381.881, end: 446.903 },
            value : "Viabilização da Equipe de Saúde da Família"
        },
        {
            time : { start : 446.904, end: 479.352 },
            value : ""
        },
        {
            time : { start : 479.353, end: 511.927 },
            value : "Responsabilidades dos municípios"
        },
        {
            time : { start : 511.928, end: 693.191 },
            value : "Distrito Sanitário Especial Indígena - DSEI"
        },
        {
            time : { start : 693.192, end: 765.096 },
            value : ""
        },
        {
            time : { start : 765.097, end : 788.286 },
            value : "O aperfeiçoamento na Lei do Mais Médicos"
        },
        {
            time : { start : 788.287, end : 811.05 },
            value : ""
        },
        {
            time : { start : 811.06, end : 852.308 },
            value : "Supervisão no PMM"
        },
        {
            time : { start : 852.309, end : 867.240 },
            value : "Tutoria no PMM"
        },
        {
            time : { start : 867.241, end : 889.92 },
            value : "Relação entre os participantes do PMM no Sistema Ensino Serviço"
        },
        {
            time : { start : 889.93, end : 943.275 },
            value : ""
        },
        {
            time : { start : 943.275, end : 1122.537 },
            value : "Projetos de Intervenção Comunitária"
        },
        {
            time : { start : 1122.538, end : 1160.950 },
            value : ""
        },
        {
            time : { start : 1160.951, end : 1224.972 },
            value : "Princípios da Medicina de Família e Comunidade"
        },
        {
            time : { start : 1224.973, end : 1251.582 },
            value : ""
        },
        {
            time : { start : 1251.583, end : 1273.437 },
            value : "Educação Permanente da equipe"
        },
        {
            time : { start : 1273.438, end : 1304.885 },
            value : ""
        },
        {
            time : { start : 1304.886, end : 1470.050 },
            value : "A importância da experiência dos profissionais"
        },
        {
            time : { start : 1470.051, end : 1566 },
            value : "A percepção dos vínculos criados"
        }
    ],

    // Complementar content
    complementarContent : [
        {
            time : { start : 0, end : 109.358 },
            title : "",
            sinopsis : "",
            showMoreModalId : false, // #modalComplementarContent_x or false
        },
        {
            time : { start : 109.359, end : 135.300 },
            title : "Acolhimento no Programa Mais Médicos",
            sinopsis : "O Acolhimento no Programa Mais Médicos consiste no processo de preparação dos médicos formados no exterior para se integrarem ao Sistema Único de Saúde (SUS)...",
            showMoreModalId : "#modalComplementarContent_1", // !TODO: modal linha 5
        },
        {
            time : { start : 135.301, end : 151.984 },
            title : "Destaque",
            sinopsis : "Veja o relato da médica cubana Adis Núbia sobre a chegada ao município.",
            showMoreModalId : "#modalComplementarContent_2",  // !TODO: modal linha 6
        },
        {
            time : { start : 151.985, end : 215.923 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 215.924, end : 235.776 },
            title : "O Agente Comunitário de Saúde",
            sinopsis : "O Agente Comunitário de Saúde (ACS) atua como elo entre a Unidade Básica de Saúde e a Comunidade.",
            showMoreModalId : "#modalComplementarContent_3", // !TODO: modal linha 10
        },
        {
            time : { start : 235.777, end : 248.872 },
            title : "Destaque",
            sinopsis : "A criação de vínculos constitui um dos fatores fundamentais ao sucesso da Atenção Básica à Saúde – ABS.",
            showMoreModalId : "#modalComplementarContent_4", // !TODO: modal linha 11
        },
        {
            time : { start : 248.873, end : 262.010 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 262.011, end : 292.24 },
            title : "Destaque",
            sinopsis : "A contratação dos profissionais no PROGRAMA MAIS MÉDICOS obedece a critérios específicos estabelecidos na Lei 12.871, de 28/10/2013.",
            showMoreModalId : "#modalComplementarContent_5", // !TODO: modal linha 13
        },
        {
            time : { start : 292.25, end : 381.880 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 381.881, end : 446.903 },
            title : "Unidade Básica de Saúde",
            sinopsis : "A Unidade Básica de Saúde (UBS) é um posto que tem como objetivo atender até 80% dos problemas de saúde da população, sem a necessidade de encaminhamento para outros serviços como emergências e hospitais...",
            showMoreModalId : "#modalComplementarContent_6", // !TODO: modal linha 17
        },
        {
            time : { start : 446.904, end : 479.352 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 479.353, end : 511.927 },
            title : "Destaque",
            sinopsis : "O Programa Mais Médicos estabelece uma série de condições para a candidatura dos municípios, dentre os quais é imprescindível a qualificação da rede...",
            showMoreModalId : "#modalComplementarContent_7", // !TODO: modal linha 19
        },
        {
            time : { start : 511.928, end : 585.750 },
            title : "Destaque",
            sinopsis : "O Distrito Sanitário EspeciaL Indígena (Dsei) consiste em uma região específica de saúde do indígena. Um Dsei contém várias aldeias e um pólo-base...",
            showMoreModalId : "#modalComplementarContent_8", // !TODO: modal linha 20
        },
        {
            time : { start : 585.751, end : 602.434 },
            title : "Destaque",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 602.435, end : 643.475 },
            title : "Destaque",
            sinopsis : "A população indígena constitui um dos grupos prioritários do atendimento do Programa Mais Médicos.",
            showMoreModalId : "#modalComplementarContent_9", // !TODO: modal linha 22
        },
        {
            time : { start : 643.476, end : 667.74 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 667.75, end : 693.191 },
            title : "Destaque",
            sinopsis : "Pelo menos 300 municípios com menos de 1 médico por 10.000 habitantes.",
            showMoreModalId : "#modalComplementarContent_10", // !TODO: modal linha 24
        },
        {
            time : { start : 693.192, end : 765.096 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 765.097, end : 788.286 },
            title : "Destaque",
            sinopsis : "Aperfeiçoamento dos profissionais participantes do Programa Mais Médicos.",
            showMoreModalId : "#modalComplementarContent_11", // !TODO: modal linha 27
        },
        {
            time : { start : 788.287, end : 811.05 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 811.06, end : 852.308 },
            title : "Supervisor pedagógico do PMM",
            sinopsis : 'A Lei do PMM define "o supervisor, profissional médico responsável pela supervisão profissional contínua e permanente do médico".',
            showMoreModalId : "#modalComplementarContent_12", // !TODO: modal linha 30
        },
        {
            time : { start : 852.309, end : 867.240 },
            title : "Tutor no PMM",
            sinopsis : 'Segundo a Lei do Programa Mais Médicos "tutor acadêmico é um docente médico responsável pela orientação acadêmica do participante do PMM."',
            showMoreModalId : "#modalComplementarContent_13", // !TODO: modal linha 31
        },
        {
            time : { start : 867.241, end : 875.164 },
            title : "Destaque",
            sinopsis : "Veja a relação entre os participantes do PMM no Sistema Ensino Serviço no Saiba Mais.",
            showMoreModalId : "#modalComplementarContent_14", // !TODO: modal linha 32
        },
        {
            time : { start : 875.165, end : 889.92 },
            title : "Projetos de Intervenção Comunitária",
            sinopsis : "Intervenção comunitária é uma proposta de trabalho na qual se elabora e desenvolve um projeto para enfrentamento de problemas de saúde da comunidade em parceria e colaboração com seus membros. Ele visa a capacitação dos indivíduos de forma a gerar mudanças contínuas e duradouras.",
            showMoreModalId : false,
        },
        {
            time : { start : 889.93, end : 943.274 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 943.275, end : 982.730 },
            title : "Medicalização Social",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 982.731, end : 1083.372 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 1083.373, end : 1122.537 },
            title : "Destaque",
            sinopsis : "Veja mais sobre a importância dos trabalhos de intervenção comunitária no Saiba Mais",
            showMoreModalId : "#modalComplementarContent_15", // !TODO: modal linha 38
        },
        {
            time : { start : 1122.538, end : 1160.950 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 1160.951, end : 1187.727 },
            title : "Longitudinalidade e Integralidade",
            sinopsis : "A longitudinalidade refere-se ao vínculo estabelecido por meio da relação terapêutica entre o paciente e os profissionais da equipe de Atenção Básica à Saúde (ABS) ao longo do tempo com os cuidados preventivos e de atenção aos episódios de doença na Unidade Básica de Saúde (UBS)...",
            showMoreModalId : "#modalComplementarContent_16", // !TODO: modal linha 40
        },
        {
            time : { start : 1187.728, end : 1224.972 },
            title : "Longitudinalidade e Integralidade",
            sinopsis : "Saiba mais sobre Longitudinalidade e Integralidade...",
            showMoreModalId : "#modalComplementarContent_17", // !TODO: modal linha 41
        },
        {
            time : { start : 1224.973, end : 1251.582 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 1251.583, end : 1273.437 },
            title : "Educação Permanente da equipe",
            sinopsis : "A Educação Permanente em Saúde (EPS) é uma estratégia regulamentada pela Portaria GM/MS no1996/2007, que dispõe sobre as diretrizes para a implementação da Política Nacional de Educação Permanente em Saúde...",
            showMoreModalId : "#modalComplementarContent_18", // !TODO: modal linha 43
        },
        {
            time : { start : 1273.438, end : 1304.885 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 1304.886, end : 1330.411 },
            title : "Destaque",
            sinopsis : "De 2013 a 2017 houve uma mudança no perfil dos médicos que entraram no Programa, com maior acessão de brasileiros nos últimos ciclos.",
            showMoreModalId : "#modalComplementarContent_19", // !TODO: modal linha 45
        },
        {
            time : { start : 1330.412, end : 1470.050 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 1470.051, end : 1489.361 },
            title : "Destaque",
            sinopsis : "A vivência dos princípios da Atenção Básica à Saúde levam à criação de vínculos diversificados.",
            showMoreModalId : "#modalComplementarContent_20", // !TODO: modal linha 50
        },
        {
            time : { start : 1489.362, end : 1531.278 },
            title : "Destaque",
            sinopsis : "O Eixo de Provimento Emergencial levou médicos a regiões desassistidas.",
            showMoreModalId : "#modalComplementarContent_21", // !TODO: modal linha 51
        },
        {
            time : { start : 1531.279, end : 1566 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
    ],

    // Locale
    locale : [
        {
            time : { start : 0, end : 109.358 },
            lat : -8.5081921,
            lng : -39.31902,
            name : 'Cabrobó - PE',
        },
        {
            time : { start : 109.359, end : 135.300 },
            lat : -8.5081921,
            lng : -39.31902,
            name : 'Bananeiras - Cabrobó - PE',
        },
        {
            time : { start : 135.301, end : 151.984 },
            lat : -8.8127004,
            lng : -40.8397427,
            name : 'Rajada - Petrolina - PE',
        },
        {
            time : { start : 151.985, end : 235.776 },
            lat : -8.5081921,
            lng : -39.31902,
            name : 'Bananeiras - Cabrobó - PE',
        },
        {
            time : { start : 235.777, end : 262.010 },
            lat : -8.5081921,
            lng : -39.31902,
            name : 'Cabrobó - PE',
        },
        {
            time : { start : 262.011, end : 292.24 },
            lat : -7.9951563,
            lng : -34.8708611,
            name : 'Olinda - PE',
        },
        {
            time : { start : 292.25, end : 349.931 },
            lat : -23.7176212,
            lng : -46.5259722,
            name : 'UBS São Pedro - São Bernardo do Campo – SP ',
        },
        {
            time : { start : 349.932, end : 369.451 },
            lat : -4.3919113,
            lng : -59.5966344,
            name : 'Borba – AM',
        },
        {
            time : { start : 369.452, end : 479.352 },
            lat : -4.0048517,
            lng : -59.1044176,
            name : 'Foz do Canumã - Borba - AM',
        },
        {
            time : { start : 479.353, end : 511.927 },
            lat : -4.3919113,
            lng : -59.5966344,
            name : 'Borba – AM',
        },
        {
            time : { start : 511.928, end : 667.74 },
            lat : -3.1354387,
            lng : -59.0628277,
            name : 'Polo Base Makira - Aldeia Correnteza - Itacoatiara – AM',
        },
        {
            time : { start : 667.75, end : 693.191 },
            lat : -15.7703033,
            lng : -47.924359,
            name : 'Brasília - DF',
        },
        {
            time : { start : 693.192, end : 739.320 },
            lat : -23.225164,
            lng : -47.2674932,
            name : 'UBS Santa Cruz - Salto - SP',
        },
        {
            time : { start : 739.321, end : 788.286 },
            lat : -8.0240466,
            lng : -34.8600452,
            name : 'UFS - Ilha do Maruim - Olinda - PE',
        },
        {
            time : { start : 788.287, end : 811.05 },
            lat : -8.5081921,
            lng : -39.31902,
            name : 'Cabrobó - PE',
        },
        {
            time : { start : 811.06, end : 834.541 },
            lat : -8.5081921,
            lng : -39.31902,
            name : 'Bananeiras - Cabrobó - PE',
        },
        {
            time : { start : 834.542, end : 852.308 },
            lat : -7.965941,
            lng : -34.9550972,
            name : 'USF Gilberto Freire - Recife – PE',
        },
        {
            time : { start : 852.309, end : 867.240 },
            lat : -8.0555435,
            lng : -34.8790755,
            name : 'RECIFE – PE',
        },
        {
            time : { start : 867.241, end : 875.164 },
            lat : -8.0240466,
            lng : -34.8600452,
            name : 'UFS - Ilha do Maruim - Olinda - PE',
        },
        {
            time : { start : 875.165, end : 889.92 },
            lat : -8.0555435,
            lng : -34.8790755,
            name : 'RECIFE – PE',
        },
        {
            time : { start : 889.93, end : 1224.972 },
            lat : -7.965941,
            lng : -34.9550972,
            name : 'USF Gilberto Freire - Recife – PE',
        },
        {
            time : { start : 1224.973, end : 1273.437 },
            lat : -8.0240466,
            lng : -34.8600452,
            name : 'UFS - Ilha do Maruim - Olinda - PE',
        },
        {
            time : { start : 1273.438, end : 1304.885 },
            lat : -9.6931235,
            lng : -35.8266082,
            name : 'Petrolina - PE',
        },
        {
            time : { start : 1304.886, end : 1330.411 },
            lat : -8.0240466,
            lng : -34.8600452,
            name : 'UFS - Ilha do Maruim - Olinda - PE',
        },
        {
            time : { start : 1330.412, end : 1375.53 },
            lat : -4.0048517,
            lng : -59.1044176,
            name : 'Foz do Canumã - Borba - AM',
        },
        {
            time : { start : 1375.54, end : 1393.224 },
            lat : -23.225164,
            lng : -47.2674932,
            name : 'UBS Santa Cruz - Salto - SP',
        },
        {
            time : { start : 1393.225, end : 1432.096 },
            lat : -7.965941,
            lng : -34.9550972,
            name : 'USF Gilberto Freire - Recife – PE',
        },
        {
            time : { start : 1432.097, end : 1531.278 },
            lat : -8.5081921,
            lng : -39.31902,
            name : 'Cabrobó - PE',
        },
        {
            time : { start : 1531.279, end : 1566 },
            lat : -8.8127004,
            lng : -40.8397427,
            name : 'Rajada - Petrolina - PE',
        },
    ],

    // Highlight dates
    highlightDates : [
        {
            time : { start : 0, end: 27.234 },
            dates : []
        },
        {
            time : { start : 27.235, end: 109.358 },
            dates : [2013]
        },
        {
            time : { start : 109.359, end: 135.300 },
            dates : [2014]
        },
        {
            time : { start : 135.301, end: 1566 },
            dates : []
        },
    ],
};