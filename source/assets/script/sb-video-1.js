/**
 * This is storyboard from video 1
 * @access protected
 * @type Object
 */
var hvStoryBoard = {
    // Titles
    chapterTitle : [
        {
            time : { start : 0, end: 99.38 },
            value : "Provocação"
        },
        {
            time : { start : 99.39, end: 163.495 },
            value : "Sistema Único de Saúde"
        },
        {
            time : { start : 163.496, end: 274.773 },
            value : "O problema da escassez de médicos"
        },
        {
            time : { start : 274.774, end: 406.405 },
            value : "Competição entre municípios pela contratação do profissional"
        },
        {
            time : { start : 406.406, end: 547.295 },
            value : "Falta de médico para contratação"
        },
        {
            time : { start : 547.296, end: 593.008 },
            value : "Criação do Programa Mais Médicos"
        },
        {
            time : { start : 593.009, end: 680 },
            value : "Importância do PMM"
        },
    ],

    // Subtitles
    chapterSubtitle : [
        {
            time : { start : 0, end: 32.23 },
            value : "Contexto de surgimento"
        },
        {
            time : { start : 32.24, end: 99.38 },
            value : "Situação da saúde antes do PMM"
        },
        {
            time : { start : 99.39, end: 133.632 },
            value : "Competências do SUS"
        },
        {
            time : { start : 133.633, end: 163.495 },
            value : "Estratégia de Saúde da família"
        },
        {
            time : { start : 163.496, end: 182.055 },
            value : "Escassez de médicos na Atenção Básica"
        },
        {
            time : { start : 182.056, end: 238.987 },
            value : "Formação de médicos inferior à demanda"
        },
        {
            time : { start : 238.988, end: 274.773 },
            value : "Alta rotatividade dos médicos"
        },
        {
            time : { start : 274.774, end: 289.121 },
            value : "Competição entre os municípios e a alta rotatividade dos médicos"
        },
        {
            time : { start : 289.122, end: 296.294 },
            value : "Dificuldade de manter um corpo clínico estável no município"
        },
        {
            time : { start : 296.295, end: 310.142 },
            value : "Perda do investimento feito na formação do profissional e na construção de vínculos"
        },
        {
            time : { start : 310.143, end: 339.296 },
            value : "Dificuldade de acesso das populações ribeirinhas ao médico"
        },
        {
            time : { start : 339.297, end: 362.652 },
            value : "Desinteresse dos médicos em trabalhar na zona rural"
        },
        {
            time : { start : 362.653, end: 389.17 },
            value : "Permanência irregular dos médicos nas UBS"
        },
        {
            time : { start : 389.18, end: 406.405 },
            value : "Dificuldade de criação de vínculos com a comunidade"
        },
        {
            time : { start : 406.406, end: 447.195 },
            value : "Cobrança da presença de médicos pela população"
        },
        {
            time : { start : 447.196, end: 476.975 },
            value : "Movimento Cadê o Médico"
        },
        {
            time : { start : 476.976, end: 486.10 },
            value : "Falta de profissional médico generalista"
        },
        {
            time : { start : 486.11, end: 501.082 },
            value : ""
        },
        {
            time : { start : 501.083, end: 547.295 },
            value : "A qualificação da rede para reduzir a dificuldade de contratação em centros urbanos"
        },
        {
            time : { start : 547.296, end: 565.730 },
            value : "Objetivos do Programa"
        },
        {
            time : { start : 565.731, end: 593.008 },
            value : "Processo de criação do Programa Mais Médicos"
        },
        {
            time : { start : 593.009, end: 642.640 },
            value : "Presença do médico em áreas antes desassistidas"
        },
        {
            time : { start : 642.641, end: 660.992 },
            value : "Estudos sobre a escassez"
        },
        {
            time : { start : 660.993, end: 680 },
            value : "O PMM como estratégia de garantia ao direito constitucional à Saúde"
        },
    ],

    // Complementar content
    complementarContent : [
        {
            time : { start : 0, end: 48.338 },
            title : "",
            sinopsis : "",
            showMoreModalId : "",
        },
        {
            time : { start : 48.339, end: 59.34 },
            title : "Agente Indígena de Saúde",
            sinopsis : "O  Agente Indígena de Saúde (AIS) é um indígena que possui função semelhante ao Agente Comunitário de Saúde e, como ele, é capacitado para atuar na prevenção de doenças e na promoção da saúde na sua própria comunidade...",
            showMoreModalId : "#modalComplementarContent_1",
        },
        {
            time : { start : 59.35, end: 64.855 },
            title : "Tuxaua",
            sinopsis : 'Tuxáua, Tuxauá, Tuxaua – Significa liderança, cacique em Tupi. Líder indígena que observa, articula, fomenta e motiva as capacidades pessoais e coletivas de seu povo..."',
            showMoreModalId : "#modalComplementarContent_2",
        },
        {
            time : { start : 64.856, end: 71.445 },
            title : "Agente Comunitário de Saúde (ACS)",
            sinopsis : "O Agente Comunitário de Saúde (ACS) é um profissional que atua como elo entre a Unidade Básica de Saúde (UBS) e a população atendida por ela. Integra a equipe de saúde, orientando os cuidados primários às famílias da sua comunidade,...",
            showMoreModalId : "#modalComplementarContent_3",
        },
        {
            time : { start : 71.446, end: 99.38 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 99.39, end: 133.632 },
            title : "Sistema Único de Saúde (SUS)",
            sinopsis : "O Sistema Único de Saúde (SUS) foi criado pela Constituição Federal de 1988 para que toda a população brasileira tenha acesso ao atendimento público de saúde. A porta de entrada do SUS é a Atenção Básica.",
            showMoreModalId : "#modalComplementarContent_4",
        },
        {
            time : { start : 133.633, end: 143.517 },
            title : "Sistema Único de Saúde (SUS)",
            sinopsis : "Equipe de Saúde da Família	Para garantir a Atenção Básica a toda a população no Brasil o Ministério da Saúde propôs a Estratégia Saúde da Família (ESF), cuja base é a Equipe de Saúde da Família(eSF)...",
            showMoreModalId : "#modalComplementarContent_5",
        },
        {
            time : { start : 143.518, end: 163.495 },
            title : "Estratégia de Saúde da Família",
            sinopsis : "A Estratégia de Saúde da Família  (ESF) substituiu o Programa Saúde da Família (PSF)  e visa à reorganização da Atenção Básica à Saúde (ABS) no País, de acordo com os preceitos do Sistema Único de Saúde...",
            showMoreModalId : "#modalComplementarContent_6",
        },
        {
            time : { start : 163.496, end: 182.055 },
            title : "Atenção Básica à Saúde",
            sinopsis : "A Atenção Básica à Saúde (ABS), também conhecida como Atenção Primária (AP), constitui um conjunto de ações de atendimento individual e coletivo e tem como objetivo resolver os problemas de saúde mais relevantes...",
            showMoreModalId : "#modalComplementarContent_7",
        },
        {
            time : { start : 182.056, end: 238.987 },
            title : "Destaque",
            sinopsis : "O Ministério da Saúde realizou em abril de 2011 um Seminário Nacional denominado Seminário Nacional sobre Escassez, Provimento e Fixação de Profissionais de Saúde em Áreas Remotas e de Maior Vulnerabilidade realizado pelo Ministério da Saúde em abril de 2011.",
            showMoreModalId : "#modalComplementarContent_8",
        },
        {
            time : { start : 238.988, end: 274.773 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 274.774, end: 310.142 },
            title : "Destaque",
            sinopsis : "Veja o Infográfico que apresenta como ocorre a competição entre os municípios e sua relação com a alta rotatividade dos médicos.",
            showMoreModalId : "#modalComplementarContent_9",
        },
        {
            time : { start : 310.143, end: 501.082 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 501.083, end: 547.295 },
            title : "Destaque",
            sinopsis : "Os centros urbanos também encontram dificuldades para contratar médicos apesar de oferecerem bons salários e condições de trabalho...",
            showMoreModalId : "#modalComplementarContent_10",
        },
        {
            time : { start : 547.296, end: 565.730 },
            title : "Destaque",
            sinopsis : "Os objetivos do Programa Mais Médicos constituem a resposta adequada às dificuldades detectadas na fase de diagnóstico do Programa.",
            showMoreModalId : "#modalComplementarContent_11",
        },
        {
            time : { start : 565.731, end: 593.008 },
            title : "Destaque",
            sinopsis : "A implantação do Programa Mais Médicos em 22 de outubro de 2013 pela Lei nº 12.871/2013 gerou manifestações favoráveis e desfavoráveis, em especial das entidades médicas...",
            showMoreModalId : "#modalComplementarContent_12",
        },
        {
            time : { start : 593.009, end: 642.640 },
            title : "",
            sinopsis : "",
            showMoreModalId : false,
        },
        {
            time : { start : 642.641, end: 660.992 },
            title : "Destaque",
            sinopsis : "2011 – A distribuição dos médicos no Brasil era muito desigual.",
            showMoreModalId : "#modalComplementarContent_13", // !TODO: Falta a referência dos documentos para os links
        },
        {
            time : { start : 660.993, end: 680 },
            title : "Destaque",
            sinopsis : "O direito à saúde está garantido na Constituição Federal de 1988. O Programa Mais Médicos atua em três frentes prioritárias para garantir esse direito.",
            showMoreModalId : "#modalComplementarContent_14", // !TODO: Falta a referência dos documentos para os links
        },
    ],

    // Locale
    locale : [
        {
            time : { start : 0, end: 48.338 },
            lat : -8.8127004,
            lng : -40.8397427,
            name : 'Rajada - Petrolina - PE',
        },
        {
            time : { start : 48.339, end: 64.857 },
            lat : -3.1354387,
            lng : -59.0628277,
            name : 'Polo Base Makira - Aldeia Correnteza - Itacoatiara – AM',
        },
        {
            time : { start : 64.856, end: 71.445 },
            lat : -4.0048517,
            lng : -59.1044176,
            name : 'Foz do Canumã - Borba - AM',
        },
        {
            time : { start : 71.446, end: 99.38 },
            lat : -8.8127004,
            lng : -40.8397427,
            name : 'Rajada - Petrolina - PE',
        },
        {
            time : { start : 99.39, end: 133.632 },
            lat : -7.9951563,
            lng : -34.8708611,
            name : 'Olinda - PE',
        },
        {
            time : { start : 133.633, end: 143.517 },
            lat : -8.5081921,
            lng : -39.31902,
            name : 'Cabrobró - PE',
        },
        {
            time : { start : 143.518, end: 163.495 },
            lat : -7.9951563,
            lng : -34.8708611,
            name : 'Olinda - PE',
        },
        {
            time : { start : 163.496, end: 182.055 },
            lat : -15.721751,
            lng : -48.0073977,
            name : 'Brasília - DF',
        },
        {
            time : { start : 182.056, end: 238.987 },
            lat : -23.8046745,
            lng : -46.6718337,
            name : 'São Bernardo Do Campo – SP',
        },
        {
            time : { start : 238.988, end: 265.430 },
            lat : -9.3727815,
            lng : -40.5275588,
            name : 'Petrolina - PE',
        },
        {
            time : { start : 265.431, end: 289.121 },
            lat : -7.9951563,
            lng : -34.8708611,
            name : 'Olinda - PE',
        },
        {
            time : { start : 289.122, end: 296.294 },
            lat : -8.1875808,
            lng : -36.1562501,
            name : 'Caruaru – PE',
        },
        {
            time : { start : 296.295, end: 310.142 },
            lat : -23.8046745,
            lng : -46.6718337,
            name : 'São Bernardo Do Campo – SP',
        },
        {
            time : { start : 310.143, end: 339.296 },
            lat : -4.3919113,
            lng : -59.5966344,
            name : 'Borba – AM',
        },
        {
            time : { start : 339.297, end: 352.992 },
            lat : -4.0048517,
            lng : -59.1044176,
            name : 'Foz do Canumã - Borba - AM',
        },
        {
            time : { start : 352.993, end: 362.652 },
            lat : -4.3919113,
            lng : -59.5966344,
            name : 'Borba – AM',
        },
        {
            time : { start : 362.653, end: 389.17 },
            lat : -4.0048517,
            lng : -59.1044176,
            name : 'Foz do Canumã - Borba - AM',
        },
        {
            time : { start : 389.18, end: 406.405 },
            lat : -4.3919113,
            lng : -59.5966344,
            name : 'Borba – AM',
        },
        {
            time : { start : 406.406, end: 433.807 },
            lat : -7.9951563,
            lng : -34.8708611,
            name : 'Olinda - PE',
        },
        {
            time : { start : 433.808, end: 447.195 },
            lat : -8.5081921,
            lng : -39.31902,
            name : 'Cabrobró - PE',
        },
        {
            time : { start : 447.196, end: 465.213 },
            lat : -7.9951563,
            lng : -34.8708611,
            name : 'Olinda - PE',
        },
        {
            time : { start : 465.214, end: 476.975 },
            lat : -9.3727815,
            lng : -40.5275588,
            name : 'Petrolina - PE',
        },
        {
            time : { start : 476.976, end: 486.10 },
            lat : -22.8777203,
            lng : -43.2475741,
            name : 'Fiocruz - Rio de Janeiro - RJ',
        },
        {
            time : { start : 486.11, end: 501.082 },
            lat : -8.5081921,
            lng : -39.31902,
            name : 'Cabrobró - PE',
        },
        {
            time : { start : 501.083, end: 547.295 },
            lat : -23.8046745,
            lng : -46.6718337,
            name : 'São Bernardo Do Campo – SP',
        },
        {
            time : { start : 547.296, end: 565.730 },
            lat : -7.9951563,
            lng : -34.8708611,
            name : 'Olinda - PE',
        },
        {
            time : { start : 565.731, end: 593.008 },
            lat : -8.5081921,
            lng : -39.31902,
            name : 'Cabrobró - PE',
        },
        {
            time : { start : 593.009, end: 642.640 },
            lat : -15.721751,
            lng : -48.0073977,
            name : 'Brasília - DF',
        },
        {
            time : { start : 642.641, end: 660.992 },
            lat : -22.8777203,
            lng : -43.2475741,
            name : 'Fiocruz - Rio de Janeiro - RJ',
        },
        {
            time : { start : 660.993, end: 680 },
            lat : -15.721751,
            lng : -48.0073977,
            name : 'Brasília - DF',
        },
    ],

    // Highlight dates
    highlightDates : [
        {
            time : { start : 0, end: 48.338 },
            dates : []
        },
        {
            time : { start : 48.339, end: 59.34 },
            dates : [1988, 1990]
        },
        {
            time : { start : 59.35, end: 99.38 },
            dates : []
        },
        {
            time : { start : 99.39, end: 133.632 },
            dates : [2012]
        },
        {
            time : { start : 133.633, end: 447.195 },
            dates : []
        },
        {
            time : { start : 447.196, end: 465.213 },
            dates : [2013]
        },
        {
            time : { start : 465.214, end: 501.082 },
            dates : []
        },
        {
            time : { start : 501.083, end: 547.295 },
            dates : [2011, 2013]
        },
        {
            time : { start : 547.296, end: 565.730 },
            dates : [2013]
        },
        {
            time : { start : 565.731, end: 593.008 },
            dates : [2013]
        },
        {
            time : { start : 593.009, end: 660.992 },
            dates : []
        },
        {
            time : { start : 660.993, end: 671.253 },
            dates : [1988]
        },
        {
            time : { start : 671.254, end: 680 },
            dates : []
        },
    ],
};