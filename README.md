
1) Instale o Node.js. Baixe em nodejs.org e faça a instalação.

2) Execute npm install -g gulp para instalar o Gulp globalmente.

3) No terminal, entre na pasta do projeto

4) Execute `npm install` na pasta do projeto para instalar as dependências do projeto

5) Para rodar o projeto, digite `gulp` no terminal.

6) Para gerar a build, digite `gulp build`.

